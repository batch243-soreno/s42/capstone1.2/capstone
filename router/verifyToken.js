const { response } = require("express");
const jwt = require("jsonwebtoken");


// ANY USER
const verifyToken = (request, response, next) => {
    const authHeader = request.headers.token;
    if(authHeader){
        const token = authHeader.split(" ")[1];
        jwt.verify(token, process.env.JWT_SEC, (error, user) => {
            if(error) response.status(403).json("Enter a valid token and proceed!");
            request.user = user;
            next();
        })
    } else {
        return response.status(401).json("You are not authenticated contact your database manager!");
    }
};

// AUTHORIZE USER
const verifyTokenAndAuthorization = (request, response, next) => {
    verifyToken(request, response, () => {

        if (request.user.id === request.params.id || request.user.isAdmin){
            next();
        } 
        else {
            response.status(403).json("You are not allowed to do that!");
        }
    });
};


// ADMIN ONLY
const verifyTokenAndAdmin = (request, response, next) => {

    verifyToken(request, response, () => {
        if (request.user.isAdmin){
            next();
        } 
        else {
            response.status(403).json("You are not allowed to do that!");
        }
    });
};

module.exports = { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin };