const express = require("express");
const app = express();
const mongoose = require("mongoose");
const dotenv = require("dotenv");
const userROute = require("./router/user");
const authROute = require("./router/auth");
const productROute = require("./router/product");
const orderROute = require("./router/order");
const cartROute = require("./router/cart");
const checkoutROute = require("./router/checkOut");
const cors = require("cors");


dotenv.config();

// MONGODB CONNECT
mongoose
    .connect("mongodb+srv://admin:admin@zuittbatch243.z9g8z4o.mongodb.net/TindahanNi?retryWrites=true&w=majority")
    .then(() => console.log("Server and database is successfully connected."))
    .catch((error) => {console.log(error);
});


// ENDPOINT API
app.use(express.json());
app.use("/user", userROute);
app.use("/auth", authROute);
app.use("/products", productROute);
app.use("/carts", cartROute);
app.use("/orders", orderROute);
app.use("/chekout", checkoutROute);



// LISTEN PORT
app.listen(process.env.PORT || 5000, () => {
    console.log(`Backend server is running fine at port: 5000 !`);
});